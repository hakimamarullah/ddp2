class Elektronik extends Barang{
	private String jenis;
	private String kondisi;
	private final double hargaLaptop = 500.0;
	private final double hargaHP = 200.0;
	private final double hargaModem = 100.0;

	public Elektronik(String jenis, String kondisi){
		this.jenis = jenis;
		this.kondisi = kondisi;
	}

	public double getValue(){
		double value =0.0;
		switch(this.jenis.toLowerCase()){
			case "laptop":
				switch(this.kondisi.toLowerCase()){
					case "buruk":
						value = 0.25 * hargaLaptop;
						break;
					case "menengah":
					value = 0.8 * hargaLaptop;
						break;
					case "baik":
						value = hargaLaptop;
						break;
					case "baru":
						value = 1.25 * hargaLaptop;
						break;
				}
				break;
			case "hp":
				switch(this.kondisi.toLowerCase()){
					case "buruk":
						value = 0.25 * hargaHP;
						break;
					case "menengah":
						value = 0.8 * hargaHP;
						break;
					case "baik":
						value = hargaHP;
						break;
					case "baru":
						value = 1.25 * hargaHP;
						break;
				}
				break;
			case "modem":
				switch(this.kondisi.toLowerCase()){
					case "buruk":
						value = 0.25 * hargaModem;
						break;
					case "menengah":
						value = 0.8 * hargaModem;
						break;
					case "baik":
						value = hargaModem;
						break;
					case "baru":
						value = 1.25 * hargaModem;
						break;
				}
				break;

		}
		return value;
	}

	public String getKondisi(){
		return this.kondisi;
	}
	public String toString(){
		return String.format("%s %s", this.jenis, this.kondisi);
	}
}