import java.util.ArrayList;
import java.util.List;
import java.lang.*;
class Kardus<T extends Barang>{
	List<T> listBarang;


	public Kardus(){
		listBarang = new ArrayList<T>();
	}
	public String rekap(){
		int countElektronik = 0;
		int countPakaian =0;
		String className="";
		for(T barang: this.listBarang){
			try{
				Elektronik castElektronik = (Elektronik)barang;
				className = "Elektronik";
				countElektronik++;
			}
			catch(Exception e){
				Pakaian castPakaian= (Pakaian)barang;
				className = "Pakaian";
				countPakaian++;
			}
		}
		if(countPakaian >0 && countElektronik >0)
			className = "Campuran";
		className = className.equals("Elektronik") ? "Elektronik":className+"\t";
		return String.format("Kardus %s\t: terdapat %d barang elektronik dan %d pakaian",className,countElektronik,countPakaian);

	}

	public double getTotalValue(){
		double total = 0;
		for(T barang : listBarang){
			total+= ((Barang) barang).getValue();
		}
		return total;
	}

	public void tambahBarang(T barang){
		this.listBarang.add(barang);
	}

	public List<T> getList(){
		return this.listBarang;
	}
}