class TabelPengiriman<T extends Barang>{
	private T[][] isiTabel;

	public TabelPengiriman(T[][] initialMatrix){
		this.isiTabel = initialMatrix;
	}

	public void tambahBarang(Barang barang, String mitraid){
		if(mitraid.equals("Sierra")){
				switch(((Elektronik)barang).getKondisi().toLowerCase()){
					case "buruk":
						this.isiTabel[3][0] = (T) barang;
						break;
					case "menengah":
						this.isiTabel[2][0] = (T) barang;
						break;
					case "baik":
						this.isiTabel[1][0] = (T) barang;
						break;
					case "baru":
						this.isiTabel[0][0] = (T) barang;
						break;
				}
		}
		else if(mitraid.equals("Delta")){
				switch(String.valueOf(((Pakaian)barang).getUkuran()).toLowerCase()){
						case "l":
							this.isiTabel[0][1] = (T) barang;
							break;
						case "m":
							this.isiTabel[1][1] = (T) barang;
							break;
						case "s":
							this.isiTabel[2][1] = (T) barang;
							break;
						}
		}
		else{
			try{
				switch(((Elektronik)barang).getKondisi().toLowerCase()){
					case "buruk":
						this.isiTabel[3][2] = (T) barang;
						break;
					case "menengah":
						this.isiTabel[2][2] = (T) barang;
						break;
					case "baik":
						this.isiTabel[1][2] = (T) barang;
						break;
					case "baru":
						this.isiTabel[0][2] = (T) barang;
						break;
				}
			}
			catch(Exception e){
				switch(String.valueOf(((Pakaian)barang).getUkuran()).toLowerCase()){
						case "l":
							this.isiTabel[0][2] = (T) barang;
							break;
						case "m":
							this.isiTabel[1][2] = (T) barang;
							break;
						case "s":
							this.isiTabel[2][2] = (T) barang;
							break;
						}
			}
		}
		

	}
	public T[][] getList(){
		return this.isiTabel;
	}
	public String rekap(){
		String[] jenisMitra ={"Sierra","Delta","Alpha"};
		String rekap ="";
		for(int i=0; i<3; i++){
			rekap += String.format("%s\t:",jenisMitra[i]);
			for(int j=0; j<4; j++){
				if(this.isiTabel[j][i] != null)
					rekap += String.format("%s/",this.isiTabel[j][i].toString());
				else
					rekap += String.format("%s/","None");
					
			}
			rekap += "\n";
		}
		return rekap;
		
	}
}