import java.util.Scanner;
public class Main{

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Kardus<Barang> campuran = new Kardus<Barang>();
		Kardus<Elektronik> elektronik = new Kardus<Elektronik>();
		Kardus<Pakaian> pakaian = new Kardus<Pakaian>();
		Barang[][] asuransi = new Barang[4][3];
		TabelPengiriman<Barang> asuransiBarang = new TabelPengiriman<Barang>(asuransi);
		int[] jumlahBarang = new int[3];

		System.out.println("Selamat datang di layanan donasi Desa Dedepe Dua!");
		System.out.println("Layanan disponsori oleh Kedai VoidMain");
		System.out.println("Ada berapa jumlah barang untuk elektronik, pakaian, dan campuran?:");
		for(int i=0; i<1; i++){
	            try{
	                String amount = in.nextLine().trim();
	                if(amount.equals(" "))
	                    throw new NumberFormatException();
	                else if(amount.split(" ").length != 3)
	                	throw new ArrayIndexOutOfBoundsException();
	                jumlahBarang[0] = Integer.parseInt(amount.split(" ")[0]);
	                jumlahBarang[1] = Integer.parseInt(amount.split(" ")[1]);
	                jumlahBarang[2] = Integer.parseInt(amount.split(" ")[2]);
	            }
	            catch(NumberFormatException e){
	                System.out.println("Integer input only");
	                i--;
	            }
	            catch(ArrayIndexOutOfBoundsException x) {
	            	 System.out.println("Format pengisian Jumlah elektronik Jumlah pakaian Jumlah campuran");
		             i--;
	            }
	            catch(java.util.NoSuchElementException y){
	                System.out.println("Keyboard Interrupt, Program has been terminated");
	                System.exit(1);
	            }
        	
		}

		//REQUEST FOR ELEKTRONICS GOODS
		System.out.println("Silahkan masukan keterangan barang elektronik");
		System.out.println("Dengan format \'jenis kondisi\' tanpa tanda \'");

		for(int i=0; i<jumlahBarang[0]; i++){
            try{
                String[]  barang = in.nextLine().split(" ");
                elektronik.tambahBarang(new Elektronik(barang[0], barang[1]));
            }
            catch(ArrayIndexOutOfBoundsException x){
                System.out.println("Please input the complete data");
                i--;
            }
            catch(java.util.NoSuchElementException y){
                System.out.println("Keyboard Interrupt, Program has been terminated");
                System.exit(1);
            }
        }

        //REQUEST FOR CLOTHES
		System.out.println("Silahkan masukan keterangan pakaian");
		System.out.println("Dengan format \'ukuran warna\' tanpa tanda \'");

		for(int i=0; i<jumlahBarang[1]; i++){
            try{
                String[]  barang = in.nextLine().split(" ");
                pakaian.tambahBarang(new Pakaian(barang[0].charAt(0), barang[1]));//Create new Pakaian Object
            }
            catch(ArrayIndexOutOfBoundsException x){
                System.out.println("Please input the complete data");
                i--;
            }
            catch(java.util.NoSuchElementException y){
                System.out.println("Keyboard Interrupt, Program has been terminated");
                System.exit(1);
            }
        }

        //REQUEST FOR MIXED BOX GOODS
		System.out.println("Silahkan masukan keterangan barang campuran");
		System.out.println("Dengan format \'ELEKTRONIK jenis kondisi\' tanpa tanda \' atau \'PAKAIAN ukuran warna\' untuk pakaian");

		for(int i=0; i<jumlahBarang[2]; i++){
			String[] barangCampuran = in.nextLine().split(" ");
			if(barangCampuran[0].equalsIgnoreCase("Elektronik")){
				campuran.tambahBarang(new Elektronik(barangCampuran[1], barangCampuran[2]));
			}
			else if(barangCampuran[0].equalsIgnoreCase("Pakaian")){
				campuran.tambahBarang(new Pakaian(barangCampuran[1].charAt(0), barangCampuran[2]));
			}
			else{
				System.out.println("Hanya bisa berdonasi barang Elektronik atau Pakaian");
				i--;
			}
		}
		in.close();
		
		//ASURANSIKAN BARANG
		for(Elektronik barang: elektronik.getList())
			asuransiBarang.tambahBarang(barang, "Sierra");
		
		for(Pakaian barang: pakaian.getList())
			asuransiBarang.tambahBarang(barang, "Delta");

		for(Barang barang: campuran.getList())
			asuransiBarang.tambahBarang(barang, "Alpha");

		
		double totalDonasi = elektronik.getTotalValue() + pakaian.getTotalValue() + campuran.getTotalValue();
		System.out.println("-----------------------------------");
		System.out.println("Terima kasih atas donasi anda");
		System.out.printf("Donasi anda sebesar %s DDD\n", totalDonasi);
		System.out.print("Rekap donasi untuk setiap kardus :\n---------------------------------\n");
		System.out.println(elektronik.rekap());
		System.out.println(pakaian.rekap());
		System.out.println(campuran.rekap());

		System.out.println("-----------------------------------");
		System.out.printf("Rekap Asuransi Mitra :\n");
		System.out.println(asuransiBarang.rekap());







	}
}