class Pakaian extends Barang{
	private char ukuran;
	private String warna;

	public Pakaian(char ukuran, String warna){
		this.ukuran = ukuran;
		this.warna = warna;
	}

	public double getValue(){
		double value =0.0;
		switch(String.valueOf(this.ukuran).toLowerCase()){
			case "l":
				value = 40.0;
				break;
			case "m":
				value = 35.0;
				break;
			case "s":
				value = 30.0;
				break;


		}
		return value;
	}

	public String getUkuran(){
		return String.valueOf(this.ukuran);
	}

	@Override
	public String toString(){
		return String.format("%s %s",String.valueOf(this.ukuran).toUpperCase(),this.warna);
	}
}