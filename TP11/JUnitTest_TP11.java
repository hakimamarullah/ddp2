import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JUnitTest_TP11 {

	@Test
	void ElektronikTestGetValueBaru() {
		Elektronik testBarang = new Elektronik("laptop","baru");
		assertEquals(testBarang.getValue(), 1.25*500.0);
	}
	@Test
	void ElektronikTestGetValueBuruk() {
		Elektronik testBarang = new Elektronik("laptop","buruk");
		assertEquals(testBarang.getValue(), 0.25*500.0);
	}
	@Test
	void ElektronikTestGetValueMenengah() {
		Elektronik testBarang = new Elektronik("laptop","menengah");
		assertEquals(testBarang.getValue(), 0.8*500.0);
	}
	@Test
	void ElektronikTestGetValueBaik() {
		Elektronik testBarang = new Elektronik("laptop","baik");
		assertEquals(testBarang.getValue(), 500.0);
	}
	
	//TEST METHOD getUkuran Class Pakaian
	@Test
	void PakaianTestUkuranL() {
		Pakaian bajuTest = new Pakaian('l',"Pink");
		assertEquals(bajuTest.getUkuran(),"l");
	}
	@Test
	void PakaianTestUkuranM() {
		Pakaian bajuTest = new Pakaian('m',"Pink");
		assertEquals(bajuTest.getUkuran(),"m");
	}
	@Test
	void PakaianTestUkuranS() {
		Pakaian bajuTest = new Pakaian('s',"Pink");
		assertEquals(bajuTest.getUkuran(),"s");
	}
	
	//TEST VALUE PAKAIAN
	@Test
	void PakaianTestValueL() {
		Pakaian bajuTest = new Pakaian('l',"Pink");
		assertEquals(bajuTest.getValue(), 40.0);
	}
	@Test
	void PakaianTestValueM() {
		Pakaian bajuTest = new Pakaian('m',"Pink");
		assertEquals(bajuTest.getValue(), 35.0);
	}
	@Test
	void PakaianTestValueS() {
		Pakaian bajuTest = new Pakaian('s',"Pink");
		assertEquals(bajuTest.getValue(), 30.0);
	}

}
