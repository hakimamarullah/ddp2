public class Ikan extends Hewan{
	public boolean isBeracun;

	public Ikan(String nama, String species, boolean isBeracun){
		super(nama, species);
		this.isBeracun = isBeracun;
	}

	@Override
	public void bergerak(){
		System.out.printf("%s bergerak dengan cara berenang", this.getNama());
	}

	@Override
	public void bernafas(){
		System.out.printf("%s bernafas menggunakan insang");
	}

	@Override
	public void bersuara(){
		System.out.printf("Blub blub blub blub. Blub. (Halo, saya %s. Saya ikan yang %s)",this.getNama(),this.isBeracun());
	}

	public String isBeracun(){
		String beracunCheck = this.isBeracun ? "beracun":"tidak beracun";
		return beracunCheck;
	}
}