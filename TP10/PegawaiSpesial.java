public class PegawaiSpesial extends Pegawai implements bisaLibur{

	public PegawaiSpesial(String nama, double uang, String levelKeahlian){
		super(nama, uang, levelKeahlian);

	}

	public void libur(){
		System.out.printf("%s sedang berlibur ke Swiss\n",this.getNama());
	}
}