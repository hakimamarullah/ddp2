public class BurungHantu extends Hewan{

	public BurungHantu(String nama, String species){
		super(nama, species);
	}

	@Override
	public void bergerak(){
		System.out.printf("%s bergerak dengan cara terbang", this.getNama());
	}

	@Override
	public void bernafas(){
		System.out.printf("%s bernafas menggunakan paru-paru");
	}

	@Override
	public void bersuara(){
		System.out.printf("Hooooh hoooooooh.(Halo, saya %s. Saya adalah burung hantu)",this.getNama());
	}
}