public class Manusia implements Makhluk {
    private String nama;
    private double uang;
    
    public Manusia(String nama, double uang){
        this.nama = nama;
        this.uang = uang;
    }
    public String getNama(){
        return this.nama;
    }
    public void bergerak(){
        System.out.println(this.nama +" bergerak dengan cara berjalan");
    }
    public void bicara(){
        System.out.println("Halo, Saya "+this.nama);
    }
    public void beli() {
        System.out.println("Perintah tidak dapat dilakukan");
    }
    public void bernafas(){

    }

    public double getUang(){
        return this.uang;
    }
}