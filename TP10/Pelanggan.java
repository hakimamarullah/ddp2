public class Pelanggan extends Manusia{
    private int NoPelanggan;
    private static int NoPelangganCounter = 0;
    public Pelanggan(String nama, double uang){
        super(nama, uang);
        this.NoPelanggan = NoPelangganCounter++;
    }

    @Override
    public void bicara(){
       System.out.printf("Halo, saya %s. Uang saya adalah %s\n",this.getNama(),this.getUang());
    }

    public void membeli(){
        System.out.printf("%s membeli makanan dan minuman dari kedai VoidMain\n",this.getNama());
    }
   
}