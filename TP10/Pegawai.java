public class Pegawai extends Manusia{
   private String LevelKeahlian;

   public Pegawai (String nama, double uang, String Level){
       super(nama, uang);
       this.LevelKeahlian = Level;
   }

   public void bicara(){
       System.out.printf("Halo, saya %s. Uang saya adalah %s, dan level keahlian saya adalah %s\n",this.getNama(),this.getUang(),this.getLevelKeahlian());
   }

   public void bekerja(){
    System.out.println(this.getNama()+" bekerja di kedai VoidMain");
   }

   public String getLevelKeahlian(){
    return this.LevelKeahlian;
   }
}
