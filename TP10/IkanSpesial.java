class IkanSpesial extends Ikan implements bisaTerbang{

	public IkanSpesial(String nama, String species, boolean beracun){
		super(nama, species, beracun);

	}

	@Override
	public void bersuara(){
		System.out.printf("Blub blub blub blub. Blub. Blub blub blub. (Halo, saya %s. Saya ikan yang %s. Saya bisa terbang loh.).\n",this.getNama(),this.isBeracun());
	}

	public void terbang(){
		System.out.println("Fwooosssshhhhh! Plup.");
	}

}