public class Main{
	public static void main(String[] args) {
		Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
		Hewan kerapu = new Ikan("Kerapu Batik", "Epinephelus Polyphekadion", false); 
		Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);
		((PegawaiSpesial)yoga).libur();
		((IkanSpesial)ikanTerbang).terbang();
		ikanTerbang.bersuara();
	}
}