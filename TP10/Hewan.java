public class Hewan implements Makhluk{
	public String nama;
	public String species;

	public Hewan(String nama, String species){
		this.nama =nama;
		this.species = species;
	}

	public void bernafas(){
		System.out.printf("%s bernafas dengan alat pernapasan",this.nama);
	}

	public void bergerak(){
		System.out.printf("%s bergerak dengan caranya masing-masing",this.nama);
	}

	public void bersuara(){
		System.out.println("KWebek kwebek kwebek...");
	}

	public String getNama(){
		return this.nama;
	}
}