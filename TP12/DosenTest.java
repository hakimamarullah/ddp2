import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class DosenTest {

	@Test
	public void DosenInstanceTest() {
		Dosen dosen = new Dosen("DosenTest","1110002385");
		assertTrue(dosen instanceof Manusia);
		assertTrue(dosen instanceof Dosen);
	}
	
	@Test
	public void DosenTest2() {
		Dosen dosen = new Dosen("DosenTest","1110002385");
		assertEquals("DosenTest",dosen.getNama());
	}
	
	@Test
	public void DosenTest3() {
		Dosen dosen = new Dosen("DosenTest","1110002385");
		assertEquals("1110002385",dosen.getNip());
	}
	
	@Test
	public void DosenTest4() {
		Dosen dosen = new Dosen("DosenTest","1110002385");
		MataKuliah kul1 = new MataKuliah("DDP2","CSGE202456");
		dosen.assignMatkul(kul1);
		
		MataKuliah[] temp = dosen.getDaftarMatkul();
		List<MataKuliah> arr = Arrays.asList(temp);
		
		assertTrue(arr.contains(kul1));
	}
	
	@Test
	public void DosenTest5() {
		Dosen dosen = new Dosen("DosenTest","1110002385");
		dosen.assignMataKuliahMahasiswa(dosen.readDataMataKuliah("CSGE601021.csv"));
		
		MataKuliah[] temp = dosen.getDaftarMatkul();
		List<MataKuliah> arr = Arrays.asList(temp);
		
		Mahasiswa[] list = arr.get(0).getDaftarMahasiswa();
		
		assertTrue(list[1].getNama().equals("Burhan"));
		assertTrue(list[0].getNama().equals("Ade Azurat"));
		assertEquals("CSGE601021", arr.get(0).getKode());
		assertEquals("Dasar-dasar Pemrograman 2", arr.get(0).getNama());
	}
}
