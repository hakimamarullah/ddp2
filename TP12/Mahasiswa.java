import java.util.ArrayList;

class Mahasiswa extends Manusia{
	private String nama;
	private String npm;
	private int jumlahMataKuliah;
	private MataKuliah[] daftarMataKuliah;
	private String[][] jadwalKuliah;
	
	public Mahasiswa(String nama, String npm){
		super(nama);
		this.npm = npm;
		daftarMataKuliah = new MataKuliah[5];
		this.jumlahMataKuliah = 0;
	}

	public void tambahMataKuliah(MataKuliah mataKuliah){
		ArrayList<MataKuliah> arr = new ArrayList<MataKuliah>();
		MataKuliah[] temp = new MataKuliah[5];
		for(MataKuliah x : this.daftarMataKuliah) {
			if(x!=null)
				arr.add(x);
		}
		
		if(!arr.contains(mataKuliah) && arr.size()<5) {
			arr.add(mataKuliah);
		}
		
		arr.remove(null);
		int count=0;
		for(int i=0; i<arr.size(); i++) {
			temp[i]= arr.get(i);
			count++;
			
		}
		this.daftarMataKuliah = temp.clone();
		this.setJumlahMataKuliah(count);
		
		
	}

	public void dropMataKuliah(MataKuliah mataKuliah) throws Exception{
		int exist=0;
		ArrayList<MataKuliah> temp = new ArrayList<MataKuliah>();
		for(int i=0; i<this.jumlahMataKuliah; i++){
			try {
				if(!(this.daftarMataKuliah[i].toString()).equals(mataKuliah.toString())){
					temp.add(daftarMataKuliah[i]);
				}
				else {
					exist++;
				}
			}
			catch(NullPointerException e) {
				continue;
			}
			
		}
		for(int i=0; i<temp.size(); i++) {
				this.daftarMataKuliah[i]=temp.get(i);
		}
		ArrayList<MataKuliah> arr = new ArrayList<MataKuliah>();
		MataKuliah[] array = new MataKuliah[5];
		for(MataKuliah x : this.daftarMataKuliah) {
			if(!arr.contains(x)) {
				arr.add(x);
			}
		}
		int count=0;
		for(int i=0; i<arr.size(); i++) {
			array[i]=arr.get(i);
			if(arr.get(i)!=null)
				count++;
		}
		this.daftarMataKuliah = array.clone();
		this.setJumlahMataKuliah(count);
		if(exist==0)
			throw new Exception("Tidak ada mata kuliah "+mataKuliah.toString());
	}
	
	private void setJumlahMataKuliah(int count) {
		this.jumlahMataKuliah=count;
		
	}

	public void printMyMatKul() {
		for(MataKuliah x : this.daftarMataKuliah) {
			try {
				System.out.printf("Mata Kuliah\t: %s\nKode\t\t: %s\n\n",x.getNama(),x.getKode());
			}
			catch(NullPointerException e) {
				continue;
			}
		}
		System.out.print("-----------------------");
		System.out.println(this.jumlahMataKuliah);
	}
	
	public String getDetail() {
		String line= "___________________________________________________________________________________________";
		String detail = "";
		int counter =0;
		detail += String.format("Nama\t\t: %s\nNPM\t\t: %s\nAngkatan\t: %s\n", this.nama,this.npm,Integer.parseInt(this.npm.substring(0,2))+2000);
		detail+= String.format("%s\n|%2s.|%-13s | %-20s | %-20s\n%s|\n",line,"No","Kode","Mata Kuliah","Keterangan",line);
		for(MataKuliah x: this.daftarMataKuliah) {
			try {
				counter++;
				detail+= String.format("|%2s.|%13s | %-20s | %-199s |\n%s|\n", counter,x.getKode(),x.getNama(),x.getKeterangan(),line);
			}
			catch(NullPointerException e) {
				counter--;
				continue;
			}
		}
		detail+= String.format("Total Mata Kuliah\t: %s\n%s|",counter, line);
		return detail;
		
	}
	
	public void printJadwal() {
		String out="";
		String line ="|___________________________________________________________________________________|";
		String[] jam = new String[]{"08.00","09.00","10.00","11.00","13.00","14.00","15.00","16.00","17.00"};
		
		out+= String.format("\t\t\t%30s\n%s\n", "Jadwal Kuliah "+this.getNama(),line.substring(1,line.length()-1));
		
		out+= String.format(" %-10s| %-9s | %-9s | %-9s | %-9s | %-9s | %-10s\n%s\n","Jam","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu",line);
		
		for(int i=0; i<this.jadwalKuliah.length; i++) {
			out+= String.format(" %-10s", jam[i]);
			for(int j=0; j<this.jadwalKuliah[i].length; j++) {
				if(this.jadwalKuliah[i][j] != null)
					out+= String.format("| %-10s", this.jadwalKuliah[i][j]);
				else
					out+= String.format("| %-10s", " ");
			}
			out+=String.format("\n%s\n", line);
		}
		System.out.println(out);
		
		
	}
	
	public String getNpm() {
		return npm;
	}

	
	public void setNpm(String npm) {
		this.npm = npm;
	}

	public void setJadwalKuliah(String[][] jadwal) {
		this.jadwalKuliah = jadwal;
	}
	
	public String[][] getJadwalKuliah(){
		return this.jadwalKuliah;
	}
	
	public MataKuliah[] getDaftarMataKuliah() {
		return daftarMataKuliah;
	}

	@Override
	public String toString(){
		return String.format("%s %s",this.getNama(),this.npm);
	}




}