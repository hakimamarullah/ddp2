import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IntegerMatrixTest {

	@Test
	public void IntegerMatrixAddTest() {
		Integer[][] m1 = new Integer[][]{{1, 2, 3}, {4, 5, 6}, {1, 1, 1}};
		Integer[][] m2 = new Integer[][]{{1, 1, 1}, {2, 2, 2}, {0, 0, 0}};
		
		// Create an instance of IntegerMatrix
		IntegerMatrix integerMatrix = new IntegerMatrix();
		Number[][] res = integerMatrix.addMatrix(m1, m2);
		
		assertTrue(compareTo(res[0][0],2));
		assertTrue(compareTo(res[0][1],3));
		assertTrue(compareTo(res[0][2],4));

		assertTrue(compareTo(res[1][0],6));
		assertTrue(compareTo(res[1][1],7));
		assertTrue(compareTo(res[1][2],8));

		assertTrue(compareTo(res[2][0],1));
		assertTrue(compareTo(res[2][1],1));
		assertTrue(compareTo(res[2][2],1));
		
	}
	
	@Test
	public void IntegerMatrixMultiplyTest() {
		Integer[][] m1 = new Integer[][]{{1, 2, 3}, {4, 5, 6}, {1, 1, 1}};
		Integer[][] m2 = new Integer[][]{{1, 1, 1}, {2, 2, 2}, {0, 0, 0}};
		
		// Create an instance of IntegerMatrix
		IntegerMatrix integerMatrix = new IntegerMatrix();
		Number[][] res = integerMatrix.multiplyMatrix(m1, m2);
		
		assertTrue(compareTo(res[0][0],5));
		assertTrue(compareTo(res[0][1],5));
		assertTrue(compareTo(res[0][2],5));

		assertTrue(compareTo(res[1][0],14));
		assertTrue(compareTo(res[1][1],14));
		assertTrue(compareTo(res[1][2],14));

		assertTrue(compareTo(res[2][0],3));
		assertTrue(compareTo(res[2][1],3));
		assertTrue(compareTo(res[2][2],3));
		
	}
	
	@Test
	public void equalsDimensionMethodTest() {
		Integer[][] m1 = new Integer[][]{{1, 2}, {4, 5, 6}, {1, 1, 1}};
		Integer[][] m2 = new Integer[][]{{1, 1, 1}, {2, 2, 2}, {0, 0, 0}};
		Integer[][] m3 = new Integer[][]{{1, 1, 1}, {2, 2, 2}, {0, 0, 0}};
		
		// Create an instance of IntegerMatrix
		IntegerMatrix integerMatrix = new IntegerMatrix();
		
		assertTrue(integerMatrix.equalsDimension(m3, m2));
		assertFalse(integerMatrix.equalsDimension(m1, m2));
	}
	
	@Test
	public void isSquareMethodTest() {
		Integer[][] m1 = new Integer[][]{{1, 2}, {4, 5, 6}, {1, 1, 1}};
		Integer[][] m2 = new Integer[][]{{1, 1, 1}, {2, 2, 2}, {0, 0, 0}};
		Integer[][] m3 = new Integer[][]{{1, 1, 1}, {2, 2, 2}, {0, 0, 0}};
		
		// Create an instance of IntegerMatrix
		IntegerMatrix integerMatrix = new IntegerMatrix();
		assertTrue(integerMatrix.isSquareMatrix(m3));
		assertTrue(integerMatrix.isSquareMatrix(m2));
		assertFalse(integerMatrix.isSquareMatrix(m1));
	}
	
	@Test
	public void findDiagonalPrimerMethodTest() {
		Integer[][] m2 = new Integer[][]{{1, 1, 1}, {2, 2, 2}, {0, 0, 0}};
		Integer[][] m3 = new Integer[][]{{1, 1, 1}, {2, 9, 2}, {2, 2, 2}};
		Integer[][] m1 = new Integer[][]{{4, 1, 1,10}, {2, 9, 2, 11}, {0, 6, 0, 13},{21,22,23,24}};
		
		
		// Create an instance of IntegerMatrix
		IntegerMatrix integerMatrix = new IntegerMatrix();
		Number[] test = integerMatrix.findDiagonalPrimer(m2);
		Number[] test2 = integerMatrix.findDiagonalPrimer(m3);
		Number[] test3 = integerMatrix.findDiagonalPrimer(m1);
		
		//TEST MATRIX m2
		assertTrue(compareTo(test[0],1));
		assertTrue(compareTo(test[1],2));
		assertTrue(compareTo(test[2],0));
		
		//TEST MATRIX m3
		assertTrue(compareTo(test2[0],1));
		assertTrue(compareTo(test2[1],9));
		assertTrue(compareTo(test2[2],2));
		
		//TEST MATRIX m3
		assertTrue(compareTo(test3[0],4));
		assertTrue(compareTo(test3[1],9));
		assertTrue(compareTo(test3[2],0));
		assertTrue(compareTo(test3[3],24));
		
	}
	
	public boolean compareTo(Number n1, Number n2) {
		return n1.doubleValue() == n2.doubleValue();
	}

}
