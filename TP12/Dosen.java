import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
class Dosen extends Manusia{
	private String nip;
	private MataKuliah[] daftarMatkul;
	
	public Dosen(String nama, String nip) {
		super(nama);
		this.nip = nip;
		daftarMatkul = new MataKuliah[4];
	}
	
	public void assignMatkul(MataKuliah mataKuliah) {
		ArrayList<MataKuliah> arr = new ArrayList<MataKuliah>();
		MataKuliah[] temp = new MataKuliah[5];
		for(MataKuliah x : this.daftarMatkul) {
			if(x!=null)
				arr.add(x);
		}
		
		if(!arr.contains(mataKuliah) && arr.size()<5) {
			arr.add(mataKuliah);
		}
		
		arr.remove(null);
		int count=0;
		for(int i=0; i<arr.size(); i++) {
			temp[i]= arr.get(i);
			count++;
			
		}
		this.daftarMatkul = temp.clone();
	}
	
	public ArrayList<String[]> readDataMataKuliah(String filename){
		ArrayList<String[]> result = new ArrayList<String[]>();
		String line="";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			while((line = br.readLine()) != null) {
				if(line != null) {
					result.add(line.split(", "));
				}
			 }
			br.close();
		}
		catch(FileNotFoundException x) {
			System.out.println("No Such Filename was Found");
		}
		catch(IOException y) {
			System.out.println("Something wrong in your file, please check your file contents");
		}
		return result;
	}
	
	public void assignMataKuliahMahasiswa(ArrayList<String[]> mataKuliah) {
		MataKuliah x = new MataKuliah(mataKuliah.get(0)[1], mataKuliah.get(0)[0]);
		for(int i=1; i<mataKuliah.size(); i++) {
			x.tambahMahasiswa(new Mahasiswa(mataKuliah.get(i)[1], mataKuliah.get(i)[0]));
		}
		
		this.assignMatkul(x);
	}
	
	public String getDetail() {
		String detail ="";
		int counter=0;
		MataKuliah[] mataKuliah = this.getDaftarMatkul();
		detail += String.format("Dosen\t\t: %s\n", this.getNama());
		
	    for(int i=0; i<mataKuliah.length; i++) {
	    	try {
				detail += String.format("MataKuliah\t: %s\nKode\t\t: %s\nPeserta\t\t:\n", mataKuliah[i].getNama(),mataKuliah[i].getKode());
					for(int j=0; j<mataKuliah[i].getDaftarMahasiswa().length; j++) {
						try {
							counter++;
							detail+= String.format("%s.%s\n",counter, mataKuliah[i].getDaftarMahasiswa()[j].toString());
						}
						catch(NullPointerException e) {
							counter--;
							continue;
						}
					}
					counter=0;
					detail +="\n";
	    	}
	    	catch(NullPointerException y) {
	    		continue;
	    	}
	    	
		}
	    return detail;
		
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public MataKuliah[] getDaftarMatkul() {
		return daftarMatkul;
	}

	public void setDaftarMatkul(MataKuliah[] daftarMatkul) {
		this.daftarMatkul = daftarMatkul;
	}
}