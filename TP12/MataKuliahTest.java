import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class MataKuliahTest {

	@Test
	public void KuliahTest1() throws Exception {
		MataKuliah kul3 = new MataKuliah("Alin","CSIE202024");
		MataKuliah kul2 = new MataKuliah("POK","CSGE202023");
		MataKuliah kul1 = new MataKuliah("DDP2","UIGE601021");
		
		assertEquals("MK Wajib Fakultas",kul2.getKeterangan());
		assertEquals("MK Peminatan Program Studi Sistem Informasi",kul3.getKeterangan());
		assertEquals("MK Wajib Universitas",kul1.getKeterangan());
	}
}
