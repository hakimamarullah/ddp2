
public class JadwalKuliah {
	 private String[][] jadwalKuliah;
	 
	 public static void main(String[] args) {
		 Mahasiswa mhs = new Mahasiswa("Hakim","1906293051");
		 Mahasiswa mhs2 = new Mahasiswa("Hakim2","1906299051");
		 JadwalKuliah x = new JadwalKuliah();
		 x.tambahJadwal("Senin", "08.00", "Matdas1");
		 x.tambahJadwal("Jumat", "08.00", "Matdas1");
		 x.tambahJadwal("Senin", "09.00", "Matdas1");
		 x.tambahJadwal("Jumat", "09.00", "Matdas1");
		 x.tambahJadwal("Senin", "10.00", "DDP2");
		 x.tambahJadwal("Kamis", "10.00", "DDP2");
		 x.tambahJadwal("Sabtu", "10.00", "BEM");
		 x.tambahJadwal("Senin", "11.00", "DDP2");
		 x.tambahJadwal("Kamis", "11.00", "DDP2");
		 x.tambahJadwal("Sabtu", "11.00", "BEM");
		 x.tambahJadwal("Rabu", "13.00", "PPW");
		 x.tambahJadwal("Jumat", "13.00", "PPW");
		 x.tambahJadwal("Rabu", "14.00", "PPW");
		 x.tambahJadwal("Jumat", "14.00", "PPW");
		 x.tambahJadwal("Selasa", "15.00", "Agama");
		 x.tambahJadwal("Jumat", "15.00", "Agama");
		 x.tambahJadwal("Selasa", "16.00", "Agama");
		 x.tambahJadwal("Jumat", "16.00", "Agama");
		 
		 mhs.setJadwalKuliah(x.getJadwal());
		 mhs2.setJadwalKuliah(x.getJadwal());
		 
		 JadwalKuliah.checkJamKosong(mhs.getJadwalKuliah(), mhs2.getJadwalKuliah());
	 }
	 
	public JadwalKuliah() {
		this.jadwalKuliah = new String[9][6];
	}
	
	public void tambahJadwal(String hari, String jam, String mataKuliah) {
		switch(jam) {
				case "08.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[0][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[0][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[0][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[0][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[0][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[0][5] = mataKuliah;
								break;
					}
					break;
				case "09.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[1][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[1][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[1][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[1][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[1][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[1][5] = mataKuliah;
								break;
					}
					break;
				case "10.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[2][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[2][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[2][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[2][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[2][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[2][5] = mataKuliah;
								break;
					}
					break;
				case "11.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[3][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[3][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[3][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[3][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[3][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[3][5] = mataKuliah;
								break;
					}
					break;
				case "13.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[4][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[4][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[4][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[4][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[4][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[4][5] = mataKuliah;
								break;
					}
					break;
				case "14.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[5][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[5][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[5][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[5][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[5][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[5][5] = mataKuliah;
								break;
					}
					break;
				case "15.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[6][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[6][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[6][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[6][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[6][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[6][5] = mataKuliah;
								break;
					}
					break;
				case "16.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[7][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[7][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[7][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[7][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[7][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[7][5] = mataKuliah;
								break;
					}
					break;
				case "17.00":
					switch(hari.toLowerCase()) {
							case "senin":
								jadwalKuliah[8][0] = mataKuliah;
								break;
							case "selasa":
								jadwalKuliah[8][1] = mataKuliah;
								break;
							case "rabu":
								jadwalKuliah[8][2] = mataKuliah;
								break;
							case "kamis":
								jadwalKuliah[8][3] = mataKuliah;
								break;
							case "jumat":
								jadwalKuliah[8][4] = mataKuliah;
								break;
							case "sabtu":
								jadwalKuliah[8][5] = mataKuliah;
								break;
					}
					break;
				default:
					System.out.print("Data jadwal tidak valid, hanya tersedia Senin-Sabtu jam 08.00-17.00");
		}
	}
	
	public String[][] getJadwal(){
		return this.jadwalKuliah;
	}
	
	public static String[][] checkJamKosong(String[][] j1, String[][] j2) {
		String out="Anda dapat bertemu pada waktu berikut :\n";
		String[][] result = new String[9][6];
		for(int i=0; i<9; i++) {
			for(int j=0; j<6; j++) {
				if(j1[i][j] == null && j2[i][j] == null) {
					switch(i) {
					case 0:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","08.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","08.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","08.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","08.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","08.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","08.00");
									break;
						}
						break;
					case 1:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","09.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","09.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","09.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","09.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","09.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","09.00");
									break;
						}
						break;
					case 2:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","10.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","10.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","10.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","10.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","10.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","10.00");
									break;
						}
						break;
					case 3:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","11.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","11.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","11.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","11.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","11.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","11.00");
									break;
						}
						break;
					case 4:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","13.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","13.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","13.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","13.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","13.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","13.00");
									break;
						}
						break;
					case 5:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","14.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","14.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","14.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","14.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","14.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","14.00");
									break;
						}
						break;
					case 6:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","15.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","15.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","15.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","15.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","15.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","15.00");
									break;
						}
						break;
					case 7:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","16.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","16.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","16.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","16.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","16.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","16.00");
									break;
						}
						break;
					case 8:
						switch(j) {
								case 0:
									out += String.format("%-10s %-5s WIB\n", "Senin","17.00");
									break;
								case 1:
									out += String.format("%-10s %-5s WIB\n", "Selasa","17.00");
									break;
								case 2 :
									out += String.format("%-10s %-5s WIB\n", "Rabu","17.00");
									break;
								case 3:
									out += String.format("%-10s %-5s WIB\n", "Kamis","17.00");
									break;
								case 4:
									out += String.format("%-10s %-5s WIB\n", "Jumat","17.00");
									break;
								case 5:
									out += String.format("%-10s %-5s WIB\n", "Sabtu","17.00");
									break;
						}
						break;
					
					}
				}
				else if(j1[i][j] == null && j2[i][j] != null){
					result[i][j] = j2[i][j];
				}
				
				else{
					result[i][j]=j1[i][j];
				}
				
			}
		}
		System.out.println(out);
		return result;
	}
}
