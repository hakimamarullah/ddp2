public class RationalMatrix extends GenericMatrix<Rational> {
	
	protected Rational add(Rational num1, Rational num2) {
		return num1.add(num2);
	}
	
	protected Rational add(Rational num1, Rational num2, Rational num3) {
		return num3.add(num1.add(num2));
	}
	
	protected Rational subtract(Rational num1, Rational num2) {
		return num1.subtract(num2);
	}
	
	protected Rational toNegative(Rational num) {
		return num.subtract(add(num,num));
	}
	
	protected Rational divide(Rational num1, Rational num2) {
		return num1.divide(num2);
	}
	
	@Override
	/** Multiply two rational numbers */
	protected Rational multiply(Rational r1, Rational r2) {
		return r1.multiply(r2);
	}
	
	protected Rational multiply(Rational r1, Rational r2, Rational r3) {
		return r3.multiply(r1.multiply(r2));
	}
	
	@Override
	/** Specify zero for a Rational number */
	protected Rational zero() {
		return new Rational(0, 1);
		}
}
