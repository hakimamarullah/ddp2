import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class MahasiswaTest {

	@Test
	public void KuliahTest() {
		Mahasiswa mahasiswa = new Mahasiswa("Hakim","1906293055");
		Mahasiswa mahasiswa2 = new Mahasiswa("Captain","20220212001");
		MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");
		
		kul1.tambahMahasiswa(mahasiswa);
		kul1.tambahMahasiswa(mahasiswa2);
		
		Mahasiswa[] daftarMhs = kul1.getDaftarMahasiswa();
	    List<Mahasiswa> arr = Arrays.asList(daftarMhs);
	 
	    assertEquals(kul1.getNama(), "DDP2");
	    assertEquals(kul1.getKode(), "CSGE601021");
	    assertTrue(arr.contains(mahasiswa));
	    assertTrue(arr.contains(mahasiswa2));

		
	}
	@Test
	public void KuliahTest2() {
		Mahasiswa mahasiswa = new Mahasiswa("Hakim","1906293055");
		Mahasiswa mahasiswa2 = new Mahasiswa("Captain","20220212001");
		MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");
		
		kul1.tambahMahasiswa(mahasiswa);
		kul1.tambahMahasiswa(mahasiswa2);
		
		mahasiswa.setNpm("1906292000380");
		mahasiswa2.setNpm("23456789102");
		
		kul1.setNama("MD2");
		
		assertEquals(kul1.getNama(),"MD2");
		assertEquals(mahasiswa.getNpm(),"1906292000380");
		assertEquals(mahasiswa2.getNpm(),"23456789102");
	}
	@Test
	public void KuliahTest3() throws Exception {
		Mahasiswa mahasiswa = new Mahasiswa("Hakim","1906293055");
		Mahasiswa mahasiswa2 = new Mahasiswa("Captain","20220212001");
		Mahasiswa mahasiswa3 = new Mahasiswa("Captain","20220212091");
		MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");
		
		kul1.tambahMahasiswa(mahasiswa);
		kul1.tambahMahasiswa(mahasiswa2);
		
		Mahasiswa[] daftarMhs = kul1.getDaftarMahasiswa();
	    List<Mahasiswa> arr = Arrays.asList(daftarMhs);
		kul1.dropMahasiswa(mahasiswa);
		kul1.tambahMahasiswa(mahasiswa3);
		
		assertTrue(arr.contains(mahasiswa3));
		assertFalse(arr.contains(mahasiswa));
	}
	
	@Test
	public void KuliahTest4() throws Exception {
		Mahasiswa mahasiswa = new Mahasiswa("Hakim","1906293055");
		MataKuliah kul2 = new MataKuliah("POK","CSGE202023");
		MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");
		
		mahasiswa.tambahMataKuliah(kul1);
		mahasiswa.tambahMataKuliah(kul2);
		
		
		MataKuliah[] daftarKul = mahasiswa.getDaftarMataKuliah();
	    List<MataKuliah> arr = Arrays.asList(daftarKul);
		mahasiswa.dropMataKuliah(kul1);
		
		assertTrue(arr.contains(kul2));
		assertFalse(arr.contains(kul1));
	}

}
