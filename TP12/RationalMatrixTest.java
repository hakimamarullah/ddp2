import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RationalMatrixTest {

	@Test
	public void RationalMatrixAddTest() {
	// Create two Rational arrays m1 and m2
	Rational[][] m1 = new Rational[3][3];
	Rational[][] m2 = new Rational[3][3];
	for (int i = 0; i < m1.length; i++)
		for (int j = 0; j < m1[0].length; j++) {
			m1[i][j] = new Rational(i + 1, j + 5);
			m2[i][j] = new Rational(i + 1, j + 6);
		 }

	// Create an instance of RationalMatrix
	RationalMatrix rationalMatrix = new RationalMatrix();
	Number[][] res = rationalMatrix.addMatrix(m1, m2);
	assertEquals("11/30",res[0][0].toString());
	assertEquals("13/42",res[0][1].toString());
	assertEquals("15/56",res[0][2].toString());

	assertEquals("11/15",res[1][0].toString());
	assertEquals("13/21",res[1][1].toString());
	assertEquals("15/28",res[1][2].toString());

	assertEquals("11/10",res[2][0].toString());
	assertEquals("13/14",res[2][1].toString());
	assertEquals("45/56",res[2][2].toString());
	}
	
	@Test
	public void RationalMatrixMultiplyTest() {
	// Create two Rational arrays m1 and m2
	Rational[][] m1 = new Rational[3][3];
	Rational[][] m2 = new Rational[3][3];
	for (int i = 0; i < m1.length; i++)
		for (int j = 0; j < m1[0].length; j++) {
			m1[i][j] = new Rational(i + 1, j + 5);
			m2[i][j] = new Rational(i + 1, j + 6);
		 }

	// Create an instance of RationalMatrix
	RationalMatrix rationalMatrix = new RationalMatrix();
	Number[][] res = rationalMatrix.multiplyMatrix(m1, m2);
	assertEquals("101/630",res[0][0].toString());
	assertEquals("101/735",res[0][1].toString());
	assertEquals("101/840",res[0][2].toString());

	assertEquals("101/315",res[1][0].toString());
	assertEquals("202/735",res[1][1].toString());
	assertEquals("101/420",res[1][2].toString());

	assertEquals("101/210",res[2][0].toString());
	assertEquals("101/245",res[2][1].toString());
	assertEquals("101/280",res[2][2].toString());
	}
	
	@Test
	public void equalsDimensionMethodTest() {
		Rational[][] m1 = new Rational[3][3];
		Rational[][] m2 = new Rational[3][3];
		Rational[][] m3 = new Rational[5][5];
		for (int i = 0; i < m1.length; i++)
			for (int j = 0; j < m1[0].length; j++) {
				m1[i][j] = new Rational(i + 1, j + 5);
				m2[i][j] = new Rational(i + 1, j + 6);
			 }
		// Create an instance of IntegerMatrix
		RationalMatrix rationalMatrix = new RationalMatrix();
		
		assertTrue(rationalMatrix.equalsDimension(m1, m2));
		assertFalse(rationalMatrix.equalsDimension(m3, m2));
	}
	

}
