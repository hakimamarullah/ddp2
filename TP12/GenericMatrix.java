public abstract class GenericMatrix<E extends Number> {
	protected abstract E add(E element1, E element2);
	protected abstract E subtract(E element1, E element2);
	protected abstract E multiply(E element1, E element2);
	protected abstract E add(E element1, E element2, E element3);
	protected abstract E multiply(E element1, E element2, E element3);
	protected abstract E toNegative(E element);
	protected abstract E divide(E element1, E element2);
	protected abstract E zero();
	
	//ADD METHOD FOR MATRICES
	public E[][] addMatrix(E[][] m1, E[][] m2) {
		
		//CHECK WHETHER THE TWO MATRICES HAVE THE SAME SIZE OR NOT
		if ((m1.length != m2.length) ||(m1[0].length != m2[0].length)) {
			throw new RuntimeException("The matrices do not have the same size");
			}
		
		E[][] result = (E[][]) new Number[m1.length][m1[0].length];
		
		for(int i=0; i<m1.length; i++) {
			for(int j=0; j<m1[0].length; j++) {
				result[i][j] = add(m1[i][j], m2[i][j]);
			}
		}
		return result;
	}
	
	//MULTIPLY METHOD FOR MATRICES
	public E[][] multiplyMatrix(E[][] m1, E[][] m2){
		//CHECH WHETHER M1'S COLUMN HAS THE SAME SIZE AS M2'S ROW
		if(m1[0].length != m2.length) {
			throw new RuntimeException("The matrices do not have compatible size");
		}
		
		E[][] result = (E[][]) new Number[m1.length][m2[0].length];
		
		// Perform multiplication of two matrices
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[0].length; j++) {
				result[i][j] = zero();
				
				for (int k = 0; k < m1[0].length; k++) {
					result[i][j] = add(result[i][j],multiply(m1[i][k], m2[k][j]));
				}
			}
		}
		return result;
	}
	
	//Dimension check
	public boolean equalsDimension(E[][] m1, E[][] m2) {
		int countTrue =0;
		if(m1.length == m2.length) {
			for(int i=0; i<m1.length; i++) {
				if(m1[i].length == m2[1].length)
					countTrue++;
			}
		}
		return countTrue == m1.length;
	}
	//Check if a matrix is square matrix
	public boolean isSquareMatrix(E[][] matrix) {
		int countTrue =0;
		for(int i=0; i<matrix.length; i++) {
			if(matrix[i].length == matrix.length)
				countTrue++;
		}
		return countTrue == matrix.length;
	}
	
	//FIND DIAGONAL PRIMER
	public E[] findDiagonalPrimer(E[][] matrix) {
		E[] result = (E[]) new Number[matrix.length];
		if(!this.isSquareMatrix(matrix))
			throw new RuntimeException("Must be NxN Matrix");
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[i].length; j++) {
				if(i==j)
					result[i] = matrix[i][j];
			}
		}
		return result;
	}
	
	public E[][] findInverse(E[][] m){
		E[][] result = (E[][]) new Number[3][3];
		if(!(this.isSquareMatrix(m)) && m.length !=3) {
			throw new RuntimeException("Only available for 3x3 Matrices");
		}
		
		E positiveSide = add(multiply(m[0][0], m[1][1], m[2][2]), multiply(m[0][1], m[1][2], m[2][0]), multiply(m[0][2], m[1][0], m[2][1]));
		E negativeSide = add(multiply(m[0][2], m[1][1], m[2][0]), multiply(m[0][0], m[1][2], m[2][1]), multiply(m[0][1], m[1][0], m[2][2]));
		
		//E constant = divide(subtract(positiveSide, negativeSide),subtract(positiveSide, negativeSide));
		//E determinan = divide(constant,subtract(positiveSide, negativeSide)); I don't know how cast it to double so i can get the true result:(
		E determinan = subtract(positiveSide,negativeSide);
		if(determinan == subtract(determinan,determinan))
			System.out.println("This matrix has no inverse");
			
		
		//FIND result
		result[0][0] = subtract(multiply(m[1][1], m[2][2]), multiply(m[1][2], m[2][1]));
		result[0][1] = toNegative(subtract(multiply(m[1][0], m[2][2]), multiply(m[1][2], m[2][0])));
		result[0][2]  = subtract(multiply(m[1][0], m[2][1]), multiply(m[1][1],m[2][0]));
		
		result[1][0]  = toNegative(subtract(multiply(m[0][1], m[2][2]), multiply(m[0][2], m[2][1])));
		result[1][1]  = subtract(multiply(m[0][0], m[2][2]), multiply(m[0][2], m[2][0]));
		result[1][2]  = toNegative(subtract(multiply(m[0][0], m[2][1]), multiply(m[0][1], m[2][0])));
		result[2][0]  = subtract(multiply(m[0][1], m[1][2]), multiply(m[0][2], m[1][1]));
		result[2][1]  = toNegative(subtract(multiply(m[0][0], m[1][2]), multiply(m[0][2], m[1][0])));
		result[2][2]  = subtract(multiply(m[0][0], m[1][1]), multiply(m[0][1], m[1][0]));
		
		for(int i=0; i<result.length; i++) {
			for(int j=0; j<result[i].length; j++)
				result[i][j] = multiply(result[i][j], determinan);
		}
		
		
		return result;
	}
	
	//DISPLAY RESULT IN MATRIX FORMAT
	public static void printResult(Number[][] m1, Number[][] m2, Number[][] result, char op) {
		for(int i=0; i<m1.length; i++) {
			for(int j=0; j<m1[0].length; j++)
				System.out.print(" "+ m1[i][j]);
			if(i == m1.length/2)
				System.out.print(" "+ op + " ");
			else
				System.out.print("    ");
			
			for(int j=0; j<m2.length; j++)
				System.out.print(" "+ m2[i][j]);
			
			if(i == m1.length/2)
				System.out.print(" "+ op + " ");
			else
				System.out.print("    ");
			
			for(int j=0; j<result.length; j++)
				System.out.print(" "+ result[i][j]);
		}
		System.out.println();
	}

}
