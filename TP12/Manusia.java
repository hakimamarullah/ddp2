class Manusia{
	private String nama;
	
	public Manusia(String nama) {
		this.nama = nama;
	}
	
	public void setNama(String nama) {
		this.nama =nama;
	}
	
	public String getNama() {
		return this.nama;
	}
	
	public String toString() {
		return "Manusia bernama "+this.nama;
	}
	
	
}