import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class MataKuliah{
	private String nama;
	private String kode;
	private int jumlahMahasiswa;
	private Dosen dosen;
	private Mahasiswa[] daftarMahasiswa;
	
	
	public MataKuliah(String nama, String kode){
		this.nama = nama;
		this.kode = kode;
		this.daftarMahasiswa = new Mahasiswa[60];
		this.jumlahMahasiswa =0;
	}

	public void tambahMahasiswa(Manusia mahasiswa){
		this.daftarMahasiswa[jumlahMahasiswa] = (Mahasiswa) mahasiswa;
		this.jumlahMahasiswa++;
	}
	public void assignDosen(Manusia dosen){
		this.dosen = (Dosen)dosen;
	}

	public void dropMahasiswa(Manusia mahasiswa) throws Exception{
		int exist=0;
		ArrayList<Mahasiswa> temp = new ArrayList<Mahasiswa>();
		for(int i=0; i<this.jumlahMahasiswa; i++){
			try {
				if(!(daftarMahasiswa[i].toString()).equals(mahasiswa.toString())){
					temp.add(daftarMahasiswa[i]);
				}
				else {
					exist++;
				}
			}
			catch(NullPointerException e) {
				continue;
			}
			
		}
		this.jumlahMahasiswa--;
		for(int i=0; i<temp.size(); i++) {
			this.daftarMahasiswa[i]=temp.get(i);
		}
		if(exist==0)
			throw new Exception("Tidak mahasiswa bernama "+mahasiswa.toString());
	}
	
	public String getKeterangan(){ 
		String keterangan = "";
		switch(this.getKode().substring(0,4)) {
				case "UIGE":
					keterangan = "MK Wajib Universitas";
					break;
				case "UIST":
					keterangan = "MK Wajib Rumpun Sains dan Teknologi";
					break;
				case "CSGE":
					keterangan = "MK Wajib Fakultas";
					break;
				case "CSCM":
					keterangan = "MK Wajib Program Studi Ilmu Komputer";
					break;
				case "CSIM":
					keterangan = "MK Wajib Program Studi Sistem Informasi";
					break;
				case "CSCE":
					keterangan = "MK Peminatan Program Studi Ilmu Komputer";
					break;
				case "CSIE":
					keterangan = "MK Peminatan Program Studi Sistem Informasi";
					break;
				default:
					keterangan ="Unknown";
					break;
		}
		return keterangan;
	}
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public Mahasiswa[] getDaftarMahasiswa() {
		return daftarMahasiswa;
	}

	public void setDaftarMahasiswa(Mahasiswa[] daftarMahasiswa) {
		this.daftarMahasiswa = daftarMahasiswa;
	}
	
	private int getJumlahMahasiswa() {
		return this.jumlahMahasiswa;
	}
	

	@Override
	public String toString(){
		return String.format("%s %s",this.kode.toUpperCase(),this.nama);
	}
}