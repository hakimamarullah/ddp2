class IntegerMatrix extends GenericMatrix<Integer> {
	protected Integer add(Integer num1, Integer num2) {
		return num1 + num2;
	}
	
	protected Integer add(Integer num1, Integer num2, Integer num3) {
		return num1 + num2 + num3;
	}
	
	protected Integer subtract(Integer num1, Integer num2) {
		return num1 - num2;
	}
	
	protected Integer toNegative(Integer num) {
		return num * -1;
	}
	
	protected Integer divide(Integer num1, Integer num2) {
		if(num2 == 0)
			throw new ArithmeticException("Zero division is not allowed");
		return num1/num2;
	}

	
	protected Integer multiply(Integer num1, Integer num2) {
		return num1 * num2;
	}
	
	protected Integer multiply(Integer num1, Integer num2, Integer num3) {
		return num1 * num2 * num3;
	}
	
	protected Integer zero() {
		return 0;
	}

}
