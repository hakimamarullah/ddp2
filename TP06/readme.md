# 💻 Tugas Pemrograman 6 (Individu)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Nama**: Hakim Amarullah | **NPM**: 1906293051 | **Kelas**: I | **Kode Tutor**: DZ

## Topik

I'm a Class. I model objects. What? 

## Dokumen Tugas

https://drive.google.com/file/d/1dlqkcR8f7zF4gfJvuQ_A-umT8ssnV6_R/view?usp=sharing

## *Checklist*

- [X] Membaca dan memahami Soal Tugas Pemrograman 6 dengan sekesama
- [X] Memahami class dan object serta penggunaannya
- [X] Melengkapi class Karyawan sesuai dengan yang diinginkan pada soal 
- [X] Melengkapi class Main sesuai dengan yang diinginkan pada soal
- [X] Mengumpulkan hasil pengerjaan ke GitLab
- [X] Melakukan peragaan Tugas Pemrograman ke Tutor