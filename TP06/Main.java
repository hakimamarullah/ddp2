import java.util.Scanner;
public class Main {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        Karyawan [] listKaryawan;
        int jumlahKaryawan = 0;
        System.out.println("Masukkan Jumlah Karyawan : ");

        for(int i=0; i<1; i++){
            try{
                String amount = scan.nextLine().trim();
                if(amount.equals(" "))
                    throw new NumberFormatException();
                jumlahKaryawan = Integer.parseInt(amount);
            }
            catch(NumberFormatException e){
                System.out.println("Integer input only");
                i--;
            }
            catch(java.util.NoSuchElementException y){
                System.out.println("Keyboard Interrupt, Program has been terminated");
                System.exit(1);
            }
        }
        /*LIST KARYAWAN*/
        listKaryawan = new Karyawan[jumlahKaryawan];

        System.out.println("Masukkan Nama, Umur , Lama Bekerja (dalam bulan): ");
        for(int i=0; i<jumlahKaryawan; i++){
            try{
                String[]  data = scan.nextLine().split(" ");
                listKaryawan[i] = new Karyawan(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]));//Create new Karyawan Object
            }
            catch(NumberFormatException e){
                System.out.println("Make sure to input integer only for Umur & Lama Bekerja");
                i--;
            }
            catch(ArrayIndexOutOfBoundsException x){
                System.out.println("Please input the complete data");
                i--;
            }
            catch(java.util.NoSuchElementException y){
                System.out.println("Keyboard Interrupt, Program has been terminated");
                System.exit(1);
            }
        }
        scan.close();
        System.out.printf("Rata-rata gaji karyawan adalah %.2f\n",rerataGaji(listKaryawan));
        System.out.printf("Karyawan dengan gaji tertinggi adalah %s\n",gajiTertinggi(listKaryawan));
        System.out.printf("Karyawan dengan gaji terendah adalah %s\n",gajiTerendah(listKaryawan));
    }

    //TODO
    public static double rerataGaji(Karyawan[] listKaryawan) {
        double gaji = 0;
        int jumlahKaryawan = listKaryawan.length;
        for(Karyawan x: listKaryawan){
            gaji += x.getGaji();
        }
        return gaji/jumlahKaryawan;
    }
    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTerendah(Karyawan[] listKaryawan) {
        double minGaji = Double.MAX_VALUE;
        String name = "";
        for(Karyawan x : listKaryawan){
            if(x.getGaji()<minGaji){
                minGaji = x.getGaji();
                name = x.getNama();
            }
        }
        return name;
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTertinggi(Karyawan[] listKaryawan) {
        double maxGaji = Double.MIN_VALUE;
        String name = "";
        for(Karyawan x : listKaryawan){
            if(x.getGaji()>maxGaji){
                maxGaji = x.getGaji();
                name = x.getNama();
            }
        }
        return name;
    }

}
