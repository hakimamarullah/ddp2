public class Karyawan {
    private String nama;
    private int umur;
    private int lamaBekerja;
    private double gaji;

    //TODO
    public Karyawan(String nama, int umur, int lamaBekerja) {
         this.nama = nama.substring(0,1).toUpperCase()+nama.substring(1).toLowerCase();
         this.umur = umur;
         this.lamaBekerja =  lamaBekerja;
         this.gaji = 100;
         if(this.umur>40){
            this.gaji+=10;//Increase by 10DDD if age>40
         }
         for(int i=0; i<lamaBekerja/3; i++)
            this.gaji += ((0.05)*this.gaji);//Increase 5% of gaji every 3 month periode kerja
    }

    //TODO
    public double getGaji(){
        return gaji;
    }

    public void setGaji(double gaji){
        this.gaji=gaji;
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama){
        this.nama=nama;
    }

    public int getUmur(){
        return this.umur;
    }

    public void setUmur(int umur){
        this.umur=umur;
    }

    public int getLamaBekerja(){
        return this.lamaBekerja;
    }

    public void setLamaBekerja(int lamaBekerja){
        this.lamaBekerja = lamaBekerja;
    }

    @Override
    public String toString(){
        return String.format("Nama\t\t: %2s\nUmur\t\t: %2d\nLama Kerja\t: %2d\nGaji\t\t: %2.2f",
            this.nama,this.umur,this.lamaBekerja,this.gaji);
    }

}
