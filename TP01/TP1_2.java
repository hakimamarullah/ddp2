// Nama		: Hakim Amarullah
// NPM		: 1906293051
// TP1		: Mencari nama fakultas berdasarkan singkatan dari user

import java.util.Scanner;
public class TP1_2 {
    public static void main(String[] args) {
        // TODO: Kerjakan Soal 2 disini!
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to DDP2 !");
        String faculty;

        /*Melakukan pencarian berulang sebelum user memerintahkan untuk keluar*/
        do{
        	System.out.println("Nama Fakultas Apa yang Ingin Anda Ketahui? ");
        	faculty = input.nextLine();
        	
	        /*Change the input toLowerCase*/
	        switch(faculty.toLowerCase()){
	        	case "fasilkom":
	        		System.out.println("Fakultas Ilmu Komputer");
	        		break;
	        	case "fk":
	        		System.out.println("Fakultas Kedokteran");
	        		break;
	        	case "fmipa":
	        		System.out.println("Fakultas Matematika dan Ilmu Pengetahuan Alam");
	        		break;
	        	case "fik":
	        		System.out.println("Fakultas Ilmu Keperawatan");
	        		break;
	        	case "fkg":
	        		System.out.println("Fakultas Kedokteran Gigi");
	        		break;
	        	case "ft":
	        		System.out.println("Fakultas Teknik");
	        		break;
	        	case "fh":
	        		System.out.println("Fakultas Hukum");
	        		break;
	        	case "fisip":
	        		System.out.println("Fakultas Ilmu Sosial Politik");
	        		break;
	        	case "fib":
	        		System.out.println("Fakultas Ilmu Budaya");
	        		break;
	        	case "feb":
	        		System.out.println("Fakultas Ekonomi dan Bisnis");
	        		break;
	        	case "fia":
	        		System.out.println("Fakultas Ilmu Administrasi");
	        		break;
	        	case "fkm":
	        		System.out.println("Fakultas Kesehatan Masyarakat");
	        		break;
	        	case "fpsi":
	        		System.out.println("Fakultas Psikologi");
	        		break;
	        	case "ff":
	        		System.out.println("Fakultas Farmasi");
	        		break;
	        	case "exit"://to avoid default case executed when user give "exit" as input
	        		break;
	        	default :
	        		System.out.println("Faculty Not Found");
	        		break;


	        }
	        
        	}
        	while(!(faculty.toLowerCase().equals("exit")));

        	System.out.println("Thank you for using our system");
	    }
	    
	}

