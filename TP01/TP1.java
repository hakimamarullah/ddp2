// Nama		: Hakim Amarullah
// NPM		: 1906293051
// TP1		: Mengecek angka dan panjang kata masukan dari user

import java.util.Scanner;
public class TP1 {
    public static void main(String[] args) {
        // TODO: Kerjakan Soal 1 disini!
        System.out.println("Welcome to DDP2 !");
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan kata ");
        String word = input.nextLine();
        System.out.println("Masukkan angka ");
        int angka = input.nextInt();
        checkNum(angka);
        checkLen(word);



    }

    /*checkNum method*/
    public static void checkNum(int x){
    	if(x%2 == 0){
    		System.out.println("This is Even Number!");
    	}
    	else if((int) Math.pow(x,2) == 4){
    		System.out.println("Ini adalah angka 2 !");
    	}
    	else{
    		System.out.println("Bukan genap dan bukan 2");
    	}
    }

    /*Word Length Checking method*/
    public static void checkLen(String x){
    	int len = x.length();
    	if(len < 5){
    		System.out.printf("Kata yang anda masukkan adalah %s\n", x);
    		System.out.println("Panjang katanya kurang dari 5");
    	}
    	else if(len > 22){
    		System.out.println("Kata yang anda masukkan sangat panjang");
    	}
    	else{
    		System.out.println("Kata yang anda masukkan biasa saja");
    	}
    }
}
