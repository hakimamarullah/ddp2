# 💻 Tugas Individu

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Nama**: Kak Burhan | **NPM**: 1906123456 | **Kelas**: J | **Kode Tutor**: RF

Repositori ini adalah tempat pengerjaan Tugas Individu untuk mata kuliah Dasar-Dasar Pemrograman 2 Semester Gasal 2020/2021.

## Cara Penggunaan

1. Buatlah satu proyek GitLab dengan nama **\[NPM\]-DDP2-TI** (Contoh: **1906123456-DDP2-TI**) di akun masing-masing dengan visibilitas **Private**.
2. Buatlah salinan proyek tersebut di komputermu dengan menggunakan <code>git clone</code>.
3. Tambahkan repositori ini sebagai *upstream* pada salinan proyek di komputermu tadi dengan menggunakan instruksi berikut ini:

   ~~~shell
   git remote add upstream https://gitlab.com/DDP2-CSUI/2020-gasal/assignments.git
   ~~~
4. Pada setiap rilis tugas pemrograman, repositori ini akan diperbarui. Untuk mengintegrasikan perubahan tersebut dengan proyekmu, gunakan instruksi berikut ini:

   ~~~shell
   git pull --allow-unrelated-histories upstream master
   ~~~
   

## Checklist

Untuk mengisi *checklist* ini, kamu dapat membuka berkas README.md melalui Visual Studio Code dan mengganti <code>[ ]</code> dengan <code>[X]</code>. GitLab akan secara otomatis mengolahnya sebagai sebuah *checklist*.

- [X] Tugas Pemrograman 1:  Hello World, Java, and Git :D
- [X] Tugas Pemrograman 2: Hi Class , I’m main
- [ ] Tugas Pemrograman 6: I'm a Class. I model objects. What? 
- [ ] Tugas Pemrograman 10: Our relationships define our lives
- [ ] Tugas Pemrograman 14: Let’s break down the problems and pack it as one


## License

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2020, Faculty of Computer Science Universitas Indonesia

Permission to copy, modify, and share the works in this project are governed under the [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause) license.