class MoviesLib{
	private String genre;
	private String negara;
	private static int callNumber = 100;
	private String callNumberID;
	private String title;
	private static String[] titleLib = new String[20000];//THIS LIBRARY CAN CONTAINS MOVIES UP TO 20000
	private static int i=0;

	/*CONSTRUCTOR*/
	public MoviesLib(String title, String genre, String negara){
		this.title = title;
		this.genre = genre;
		this.negara = negara;
		this.callNumberID = String.valueOf(callNumber);
		titleLib[i]= title;
		i++;
		callNumber++;
	}
	public MoviesLib(String title, String negara){
		this.title = title;
		this.negara=negara;
		this.genre ="Unknown";
		this.callNumberID = String.valueOf(callNumber);
		titleLib[i]= title;
		i++;
		callNumber++;
	}

	/*DISPLAY LIBRARY*/
	public static void showMoviesList(){
		System.out.println("Available Movies :");
		int i=1;
		for(String title: titleLib){
			try{
				if(!title.equals(null)){
					System.out.println(i +". "+title);
					i++;}
			}
			catch(NullPointerException err){
				break;
			}
		}
	}

	/*EDIT GENRE*/
	public void setGenre(String newGenre){
		this.genre=newGenre;
	}

	/*EDIT MOVIES*/
	public void setTitle(String title){
		this.title = title;
	}
	/*GET GENRE*/
	public String getGenre(){
		return this.genre;
	}
	/*GET TITLE*/
	public String getTitle(){
		return this.title;
	}
	/*GET COUNTRY*/
	public String getCountry(){
		return this.negara;
	}
	/*GET CALLNUMBER*/
	public String getCallNumber(){
		return this.callNumberID;
	}

	@Override
	public String toString(){
		return String.format("Title\t\t: %s\nGenre\t\t: %s\nCountry\t\t: %s\nCall Number\t: %s",this.title,this.genre,this.negara,this.callNumberID);
	}
}