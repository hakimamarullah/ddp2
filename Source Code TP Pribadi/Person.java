class Person{
		private String firstname;
		private String lastname;
		private int birthmonth;
		private int birthday;
		private int birthyear;
		private String ssn;

		/*CONSTRUCTOR*/
		public Person(String firstname, String lastname, int birthmonth, int birthday, int birthyear, String ssn){
			this.firstname = firstname;
			this.lastname = lastname;
			this.birthmonth = birthmonth;
			this.birthday = birthday;
			this.birthyear = birthyear;
			this.ssn = ssn;
		}

		public void setFirstname(String firstname){
			this.firstname = firstname;
		}

		public void setLastname(String lastname){
			this.lastname = lastname;
		}
		public String getFirstname(){
			return this.firstname;
		}
		public String getLastname(){
			return this.lastname;
		}

		public String getBirthday(){
			return String.format("%s/%s/%s",this.birthmonth,this.birthday,this.birthyear);
		}	

		public boolean verifySSN(String ssn){
			return ssn.equals(this.ssn) ? true : false;
		}
	
		@Override
		public String toString(){
			return String.format("Full Name\t: %s %s\nBirthday\t: %s",this.firstname,this.lastname,this.getBirthday());
		}


}