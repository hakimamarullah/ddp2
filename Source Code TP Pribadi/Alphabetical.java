import java.util.Scanner;
class Alphabetical{
	public static void main(String[] args) {
		 Scanner in = new Scanner(System.in);
		 System.out.println("Enter a String to sort alphabetically");
		 String str = in.nextLine();
		 in.close();
		 System.out.println(alphabetical(str));
	
	}
	public static String alphabetical(String str){
		String res="";
		java.util.ArrayList<Integer> y = new java.util.ArrayList<Integer>();
		for(int i=str.length()-1;i>0;i--)
			for (int j=0;j<i ;j++ ) {
				if(str.charAt(i)<str.charAt(j)){
					y.add(i);
					break;

				}
			}
	
		java.util.ArrayList<Character> x = new java.util.ArrayList<Character>();
		for(int i=0;i<str.length();i++)
			x.add(str.charAt(i));

		for(int i=0;i<x.size();i++)
			for(int j=0; j<y.size();j++){
				if(i==y.get(j))
					x.set(i,' ');
			}

		for(Character z:x){
			if(z!=' ')
				res+=z;
		}
		return res;
	}

}