public class Sort{
	public static void main(String[] args) {
		int [] num = {10,4,7,2,9,23,4,0,19};
		sort(num);
		for(int x:num)
			System.out.print(x+" ");

	}
	public static void sort(int[] num){
		int currentMin;
		int indexMin;
		for(int i =0; i<num.length; i++){
			currentMin=num[i];
			indexMin=i;
			for(int j=i+1; j<num.length; j++){
				if(currentMin > num[j]){
					currentMin=num[j];
					indexMin=j;
				}
			}
			if(indexMin != i){
				num[indexMin]=num[i];
				num[i]=currentMin;
			}
		}
	}
}