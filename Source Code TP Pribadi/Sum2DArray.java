class Sum2DArray{
		public static void main(String[] args){
		int[][] a = {
			{-5,-2, 2},
			{1,-5, 3}
		};
		int[][] b = {
		    {1,1,0,1},
		    {1,1,1,1}
		};
		int[][] result = sumArray(a,b);
		//prints array, should match example on right
		for(int[] row : result){
			for(int e : row){
				System.out.print(e + " ");
			}
			System.out.println();
		}
	}
	/*METHOD TO SUM*/
	public static int[][] sumArray(int[][] arr1, int[][] arr2){
		 int[][] newArray=new int[Math.max(arr1.length,arr2.length)][Math.max(arr1[0].length,arr2[0].length)];
		 for(int i =0; i< newArray.length;i++){
		 	for (int j=0;j<newArray[i].length ; j++ ) {
		 		if(i<Math.min(arr1.length,arr2.length)){
			 		try{
			 			newArray[i][j] = arr1[i][j] + arr2[i][j];
			 		}
			 		catch(IndexOutOfBoundsException e){
		 				try{
		 					newArray[i][j]=arr1[i][j];
		 				}
		 				catch(IndexOutOfBoundsException x){
		 					newArray[i][j]=arr2[i][j];
		 					}
			 		}
		 		}
		 		else if(i>=Math.min(arr1.length,arr2.length)){
		 			
		 			try{
			 			newArray[i][j] = arr1[i][j];
			 		}
			 		catch(IndexOutOfBoundsException e){
		 				try{
		 					newArray[i][j]=arr2[i][j];
		 				}
		 				catch(IndexOutOfBoundsException x){
		 					newArray[i][j]=0;
		 					}
			 		}
		 		}
		 		
		 	}
		 }
		 return newArray;
		
	}
}