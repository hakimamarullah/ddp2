class Deck{
	private Card[] deck;
	private String[] suits ={"Club","Spade","Heart","Diamond"};
    private String [] ranks = {"Ace","2","3","4","5","6","7","8","9","10","King", "Queen", "Jack" };

	/*CONSTRUCTOR*/
	public Deck(){
		deck = new Card[52];
		int currentIndex =0;
		for (int suit=0; suit<=3 ;suit++ ) {
			for (int rank=0; rank<=12 ;rank++ ) {
				deck[currentIndex]=new Card(suits[suit],ranks[rank],currentIndex);
				currentIndex++;
				
			}
			
		}
			
	}

	public Card draw(){
		return deck[0];
	}
	public void shuffle(){
		for(int i=0; i<deck.length; i++){
			int index = (int) Math.floor(Math.random()*deck.length);
			Card temp = deck[i];
			this.deck[i] = deck[index];
			this.deck[index] = temp;
		}
	}
}