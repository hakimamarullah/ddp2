class RowSum{
	public static void main(String[] args) {
		int[][] a = {
			{1,1,1,1},
			{1,1,1,1},
			{1,1,1,1}
		};
	}
	public static int[] rowSums(int[][] x){
		int[] res = new int[x.length];
		int count =0;
		for (int i=0; i<x.length ; i++) {
			for (int j=0; j<x[i].length ; j++ ) {
				count+= x[i][j];
				
			}
			res[i]=count;
			count=0;
		}
		return res;
	}
}