class Card{
	private String rank;
	private String suit;
	private int value;

	/*CONSTRUCTOR*/
	public Card(String suit, String rank, int value){
		this.rank = rank;
		this.suit = suit;
		this.value = value;
	}
	/*GETTER*/
	public String getName(){
		return this.rank;
	}

	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}

	@Override
	public String toString(){
		return String.format("%s of %s",this.rank,this.suit);
	}

}