class countVowels{
    public static void main(String[] args) 
	{
		String[] arr = {"hey","yolo","hi","this is long"};
		System.out.println(minVowels(arr));
		//should print "hi"
	}
	public static String minVowels(String[] words)
	{int min = countVowels(words[0]);
	String shortest = words[0];
	for(int i=1; i<words.length; i++)
	  if(countVowels(words[i])<=min && words[i].length()<shortest.length()){
	    min = countVowels(words[i]);
		shortest = words[i];
	  }
	 return shortest;
		
	}
	public static int countVowels(String s)
	{int count=0;
	  for(int i=0; i<s.length(); i++){
	    if(isVowel(s.charAt(i)))
	      count++;
	  }
	return count;
	}
	public static boolean isVowel(char ch)
	{
		return ch == 'a' ||
			   ch == 'e' ||
			   ch == 'i' ||
			   ch == 'o' ||
			   ch == 'u';
	}
}
