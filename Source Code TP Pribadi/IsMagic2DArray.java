class IsMagic2DArray{
	public static void main(String[] args) {
		int[][] a = {
			{1,1,1},
			{1,1,1},
			{1,1,1}
		};
		System.out.println(isMagic(a)); //true
		int[][] b = {
			{1,1,2},
			{3,2,3},
			{1,4,1}
		};
		System.out.println(isMagic(b)); //false
		int[][] c = {
			{1,1,1},
			{2,2,2}
		};
		System.out.println(isMagic(c)); //false
		int[][] d = {
			{8,1,6},
			{3,5,7},
			{4,9,2}
		};
		System.out.println(isMagic(d)); //true
	}
	public static boolean isMagic(int[][] array){
		int rowSum =0;
		int colSum =0;
		int diagonalSum =0;
		int colIndex=0;
		boolean square = true;
		boolean magic =true;
		for(int[] row : array){
			square=row.length%array.length ==0;
		}
		if(square){
			for (int i=0; i<array.length ; i++) {
				rowSum=0;
				for (int j=0; j<array[i].length ; j++ ) {
					rowSum+= array[i][j];
					
					
				}
				colSum+= array[i][colIndex];
				diagonalSum += array[i][i];
				magic = colSum == rowSum && colSum==diagonalSum && rowSum==diagonalSum;
				colIndex++;

				
			}
		}
		return magic && square;
	}
		
}
