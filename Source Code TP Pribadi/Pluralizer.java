class Pluralizer{
	public static void main(String[] args) {
		System.out.println(pluralize("cat"));
	}
	public static String pluralize(String x){
		String CONSONANT = "bcdfghjklmnpqrstvwxyz";
		String exceptO = "photo piano halo";
		if(x.endsWith("s") ||x.endsWith("us") || x.endsWith("ss") || x.endsWith("is") || x.endsWith("sh") || x.endsWith("ch") || x.endsWith("x") || x.endsWith("z") || x.endsWith("o") ){
			if(exceptO.contains(x)){
				return x+"s";
			}
			else if(x.endsWith("us")){
				return x.substring(0,x.length()-2)+"i";
			}
			else if(x.endsWith("is")){
				return x.substring(0,x.length()-2)+"es";
			}
			else {
				return x+"es";
			}
		}
		else if(x.endsWith("f")||x.endsWith("fe")){
			if( x.endsWith("of") || x.endsWith("ef")){
				return x+"s";
			}
			else if(x.endsWith("f")){
				return x.substring(0,x.length()-1)+"ves";
			}
			else{
				return x.substring(0,x.length()-2)+"ves";
			}
			
		}
		else if(x.endsWith("y")){
			if(CONSONANT.contains(x.substring(x.length()-2,x.length()-1))){
				return x.substring(0,x.length()-1)+"ies";
			}
			else{
				return x+"s";
			}
		}
		else if(x.endsWith("on")){
			return x.substring(0,x.length()-2)+"a";
		}
		return x+"s";
	}
}
