public class Stock{
	/*CLASS VARIABEL*/
	public String tickerSymbol;
	public String companyName;
	public int price; 
	public double percentChange;
	public int totalShares;
	public long marketCap;

	/*CONSTRUCTOR*/
	public Stock(String tickerSymbol, String companyName, int price, int totalShares){
		this.tickerSymbol = tickerSymbol.toUpperCase();
		this.companyName = companyName;
		this.price = price;
		this.percentChange = 0;
		this.totalShares = totalShares;
		this.marketCap = (long)this.totalShares*(long)this.price;
	}
	/*ADJUST PRICE*/
	public void adjustPrice(int change){
		int oldPrice = this.price;
		this.percentChange = (((double)(change-oldPrice))/(double) oldPrice)*100;
		this.price = change;
		this.marketCap = (long)change*(long)this.totalShares;
	}
	@Override
	public String toString(){
		return String.format("Ticker Symbol\t:%s\nCompany\t\t:%s\nCurrent Price\t:$%s (%s%4.1f%%)\nMarket Cap\t:$%s", this.tickerSymbol,this.companyName,
			this.price,this.percentChange>0?"+":"",this.percentChange,this.marketCap);
	}
}