import java.util.*;
public class Multiply{
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = Integer.parseInt(in.nextLine());
		for(int i=0; i<=n*2; i++){
			System.out.printf(" ");
		}
		System.out.printf("Tabel Perkalian %d x %d\n",n,n);
		for(int i=n; i>0; i--){
			System.out.printf("%2s%4d","",i);
			
		}
		System.out.println();
		for(int i=0; i<=n*3; i++){
			System.out.print("- ");
		}
		System.out.println();
		for(int i=1; i<=n; i++){
			System.out.printf("%d|",i);
			for(int j=n; j>=i; j--){
				System.out.printf("%4d  ", i*j);
			}
			System.out.println();
		}

	}
}