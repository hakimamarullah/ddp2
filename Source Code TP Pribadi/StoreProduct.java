class StoreProduct{
  public String label;
  public int price;
  public String category;
  public boolean hasExpiration;
  public int stock;
  
  public StoreProduct(String label, int price, String category, boolean hasExpiration, int stock){
    this.label = label;
    this.price = price;
    this.category = category;
    this.hasExpiration = hasExpiration;
    this.stock= stock;
  }
 public StoreProduct(String label, int price,int stock){
    this.label = label;
    this.price = price;
    this.category = "misc";
    this.hasExpiration = false;
    this.stock= stock;
  }
public StoreProduct(String label, int price){
    this.label = label;
    this.price = price;
    this.category = "misc";
    this.hasExpiration = false;
    this.stock= 0;
  }
public StoreProduct(String label, int price, String category, boolean hasExpiration){
    this.label = label;
    this.price = price;
    this.category = category;
    this.hasExpiration = hasExpiration;
    this.stock= 0;
  }
/*Chack hasExpired*/
public void hasExpired(boolean hasExpired){
	if(hasExpired && hasExpiration)
		this.stock=0;
}
/*Check if the stock enough to sale*/
public boolean sale(int quantity){
	if(this.stock>quantity){
		this.stock-=quantity;
	}
	return this.stock>quantity;
}
/*Calculate discount*/
public double getDiscountedPrice(double discount){
	return this.price * (1-discount);
}
@Override
public String toString(){
    return String.format("Label\t\t: %2s\nPrice\t\t: %2d\nCategory\t: %2s\nhasExpiration\t: %2s\nStock\t\t: %2d\n",this.label,this.price,this.category,
    	this.hasExpiration,this.stock);
  }
}