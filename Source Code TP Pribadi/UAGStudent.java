class UAGStudent{
		public String name;
		public int grade;
		public static String principalName = "Ms. McKoy";
		public String studentID;
		public static int nextID=100;

		/*CONSTRUCTOR*/
		public UAGStudent(String name, int grade){
			this.name = name;
			this.grade = grade;
			this.studentID = (name.substring(0,1)).toUpperCase()+nextID;
			nextID++;
		}
		/*METHOD newPrincipal*/
		public static void newPrincipal(String newPrincipal){
			principalName = newPrincipal;
		}

		/*RESET ID*/
		public static void resetID(){
			nextID =100;
		}

		/*toString*/
		public String toString(){
			return String.format("%s %s",this.name,this.studentID);
		}
}