//Nama	: Hakim Amarullah
//NPM	: 1906293051
//Tugas	: TP2_3
//Specs	: Dedepedua's Visitor Authentication
import java.util.Scanner;
public class TP2_3{
	public static void main(String[] args) {
		final String favPet = "burung hantu";
		final int a=8, b=10, c=18;//Amount of Harimau
		final double minSpeed = 100.0;
		final double maxSpeed = 120.0;
		String userPetInput;
		int harimauAmount;
		double speed;
		Scanner in = new Scanner(System.in);
		System.out.println("Selamat datang di desa Dedepedua");
		System.out.println("Apa hewan kesukaan Kepala Desa?");
		System.out.println("Berapa jumlah harimau di desa Dedepedua?");
		System.out.println("Berapa kecepatan harimau di desa Dedepedua?");
		//REQUEST INPUT
		userPetInput = in.nextLine();


		//Authenticating
		if (userPetInput.equals(favPet) && !userPetInput.equals(" ")) {
			harimauAmount = Integer.valueOf(in.nextLine());
			if((harimauAmount==a || harimauAmount==b || harimauAmount==c) &&  !Double.isNaN(harimauAmount)){
				speed =Double.valueOf(in.nextLine());
				if(speed>minSpeed && speed<maxSpeed && Double.isNaN(speed)){
					System.out.println("Selamat kamu boleh masuk");
				}
				else{System.out.println("Kamu dilarang masuk");}
			}
			else{System.out.println("Kamu dilarang masuk");}
		}

		else{
			System.out.println("Kamu dilarang masuk");}
		in.close();
	}
}