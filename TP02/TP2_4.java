//Nama	: Hakim Amarullah
//NPM	: 1906293051
//Tugas	: TP2_4
//Specs	: Identify right triangles to find intruders
import java.util.Scanner;
import java.util.Arrays;
public class TP2_4{
	public static void main(String[] args) {
		int longestSide, shortestSide, mediumSide;
		int ab,bc,ac;
		int[] side = new int[3];
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the length of ab, bc, ac : ");
		ab = in.nextInt();
		bc = in.nextInt();
		ac = in.nextInt();
		in.close();
		side[0]=ab;
		side[1]=bc;
		side[2]=ac;
		Arrays.sort(side);

		/*Classify the side length*/
		longestSide=side[2];
		mediumSide=side[1];
		shortestSide=side[0];
		
		/*Inspect and print result*/
		if (isRightTriangles(longestSide,mediumSide,shortestSide)) {
			System.out.println("===== ALERT =====");
			System.out.println("segitiga siku-siku, mereka penyusup");
			System.out.printf("%s pemimpin mereka",findIntruders(longestSide,ab,bc));
		}
		else{
			System.out.println("bukan segitiga siku-siku, mereka bukan penyusup");
		}


	}

	/*Check whether that is right triangles or not*/
	public static boolean isRightTriangles(int longest, int medium, int shortest){
		int x = (int)Math.pow(longest,2);
		int y = (int)Math.pow(medium,2);
		int z = (int)Math.pow(shortest,2);
		return (y+z)==x;
	}

	/*Find the intruders*/
	public static String findIntruders(int longestSide, int ab, int bc){
		String out = ab == longestSide ? "c" : bc==longestSide ? "a" : "b";
		return out;
	}
}